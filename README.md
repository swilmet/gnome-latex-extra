Enter TeX - extra repository
============================

Additional files related to
[Enter TeX](https://gitlab.gnome.org/swilmet/enter-tex) that don't fit in the
main repository, to keep a small size for the main repository.
